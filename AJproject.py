import csv
import pickle
import matplotlib.pyplot as plt #https://code.visualstudio.com/docs/python/python-tutorial
import datetime
#Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope Process

class Cow:    #class for all of the cows in the herd
    def __init__(self, code, UKEarTag, DamEarTag, Group, cell_count_date, cell_count, milk_date, milk_amount):
        
        self.Code = code
        self.UKEarTag = UKEarTag
        self.DamEarTag = DamEarTag
        self.Group = Group
        # self.reg_no = reg_no
        # self.collar_no = collar_no
        self.cell_count_group_date = cell_count_date
        self.cell_count_group = cell_count
        # self.last_cell_date = last_cell_date
        self.milking_date = milk_date
        self.milk_amount = milk_amount
        # self.medical_intervention = medical_intervention
        # self.med_int_date = med_int_date
        # self.calving_date = calving_date
        # self.mother = mother
        # self.alive = alive

    def __str__(self):
        return "{} with UK ear tag {} with milk amounts: {} on dates of {}".format(self.Code, self.UKEarTag, self.milk_amount, self.milking_date)


def import_cow_data(file):
    cow_data = list(csv.reader(open(file)))
    create_cow_objects(cow_data)

def create_cow_objects(cow_data):
    cow_list = []
    for cow in cow_data:
        new_cow = Cow(cow[1], cow[0], cow[3], cow[2], [],[],[],[])
        cow_list.append(new_cow)
    import_milk_data_to_objects(cow_list)

def import_milk_data_to_objects(cow_list):
    milk_data = list(csv.reader(open('milkdata.csv')))
    for cow in cow_list:
        #extract the dates for each milking and place in each object.
        cow.milking_date = milk_data[0]
    for cow in cow_list:
        #extract the milk amounts for each cow based on their code.Pop the first item in the
        #milking because it is the cow code. Cow milk data will have "code" removed as well
        for milking in milk_data:
            if cow.Code == milking[0]:
                milking.pop(0)
                cow.milk_amount = milking
    print(cow.milking_date, cow.milk_amount)
    plot_a_graph(cow_list)
    # create_dictionary_of_objects(cow_list)


# def create_dictionary_of_objects(cow_list):
#     cow_dictionary = dict([(cow.Code, cow) for cow in cow_list])
#     cow = input("enter the cown number")
    
#     cow_dictionary[cow].cell_count_group.append(6)
#     print(cow_dictionary[cow].cell_count_group)
    # for cow in cow_dictionary:
    #     print (cow, cow_dictionary[cow])
    

def plot_a_graph(cow_list):
    #enter your own cow code
    cow_code = int(input("enter the cow code"))
    cow_data = cow_list[cow_code]
    dates = cow_data.milking_date
    #convert the dates from strings to date datatypes
    dates_list = [datetime.datetime.strptime(date, '%d/%m/%Y').date() for date in dates]
    print(dates_list)

    #check if the cow has any data. If it has replace with 0.0
    if cow_data.milk_amount[0] == "":
        for i in range(len(cow_data.milk_amount)):
            cow_data.milk_amount[i] = 0.0


    #convert the milkvalues from strings to floats

    milk_values = list(map(float, cow_data.milk_amount))
    print(milk_values)
    label = "UK Ear Tag: "+ cow_data.UKEarTag+", cow code:"+ cow_data.Code 
    #draw the raw plot
    plt.plot(dates_list, milk_values)
    plt.xlabel('date')
    plt.xticks(rotation='vertical')
    plt.ylabel('milk production')
    plt.title(label)
    plt.show()



def save_cow_objects(cow_list):
    with open('cow_objects.pkl', 'wb') as f:
        pickle.dump(cow_list, f)

def get_cow_objects():
    cow_objects = []
    with open("cow_objects.pkl", "rb") as f:
        while True:
            try:
                cow_objects.append(pickle.load(f))
            except EOFError:
                break
    return cow_objects 



import_cow_data("cowinfo.csv")









